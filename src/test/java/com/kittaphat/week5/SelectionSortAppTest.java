package com.kittaphat.week5;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class SelectionSortAppTest {
    @Test
    public void shoundFindMinIndexTestCase1() {
        int arr[] = {5, 4, 3, 2, 1};
        int pos = 0;
        int minIndex = SelectionSortApp.findMinIndex(arr, pos);
        assertEquals(4, minIndex);
    }

    @Test
    public void shoundFindMinIndexTestCase2() {
        int arr[] = {1, 4, 3, 2, 5};
        int pos = 1;
        int minIndex = SelectionSortApp.findMinIndex(arr, pos);
        assertEquals(3, minIndex);
    }

    @Test
    public void shoundFindMinIndexTestCase3() {
        int arr[] = {1, 2, 3, 4, 5};
        int pos = 2;
        int minIndex = SelectionSortApp.findMinIndex(arr, pos);
        assertEquals(2, minIndex);
    }

    @Test
    public void shoundFindMinIndexTestCase4() {
        int arr[] = {1,1,1,1,1,0,1,1};
        int pos = 0;
        int minIndex = SelectionSortApp.findMinIndex(arr, pos);
        assertEquals(5, minIndex);
    }

    @Test
    public void shoundSwapTestCase1() {
        int arr[] = {5, 4, 3, 2, 1};
        int expecteds[] = {5, 4, 3, 2, 1};
        int first = 0;
        int second = 0;
        SelectionSortApp.swap(arr, first, second);
        assertArrayEquals(expecteds, arr);
    }

    @Test
    public void shoundSelectSortTestCase1() {
        int arr[] = {5, 4, 3, 2, 1};
        int sortedArr[] = {1,2,3,4,5};
        SelectionSortApp.selectionSort(arr);
        assertArrayEquals(sortedArr, arr);
    }

    @Test
    public void shoundSelectSortTestCase2() {
        int arr[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
        int sortedArr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        SelectionSortApp.selectionSort(arr);
        assertArrayEquals(sortedArr, arr);
    }

    @Test
    public void shoundSelectSortTestCase3() {
        int arr[] = {6, 9, 3, 7, 10, 5, 4, 8, 2, 1 };
        int sortedArr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        SelectionSortApp.selectionSort(arr);
        assertArrayEquals(sortedArr, arr);
    }

}
